# -*- coding: utf-8 -*-
import sys
from typing import Dict


class FsmItem:
    def __init__(self, current_state, next_state, transition_function, transition_verify_function,
                 on_transition_fail_function=None, action_function=None):
        self.current_state = current_state
        self.next_state = next_state
        self.transition_function = transition_function
        self.transition_verify_function = transition_verify_function
        self.on_transition_fail_function = on_transition_fail_function
        self.action_function = action_function


class MainFsm:
    def __init__(self, app):
        self.app = app
        self.states: Dict[int, FsmItem] = {}
        self.current_state_id = None

    def add_state(self, current_state, next_state, transition_function, transition_verify_function,
                  on_transition_fail_function=None, action_function=None):
        self.states[current_state] = FsmItem(current_state, next_state, transition_function,
                                             transition_verify_function, on_transition_fail_function, action_function)

    def set_state(self, state_id):
        self.current_state_id = state_id

    def get_current_state(self):
        return self.states[self.current_state_id]

    def _get_next_state_id(self):
        return self.get_current_state().next_state

    def get_next_state(self):
        return self.states[self._get_next_state_id()]

    def next(self):
        next_state_id = self._get_next_state_id()
        print("current state {}, next state {}".format(self.current_state_id, next_state_id))
        next_state = self.get_next_state()
        if next_state.transition_function is not None:
            next_state.transition_function()
        if next_state.transition_verify_function is not None:
            check_passed = next_state.transition_verify_function()
            if not check_passed:
                if next_state.on_transition_fail_function is not None:
                    next_state.on_transition_fail_function()
                else:
                    print("check not passed!")
                    # where_am_i()
        if next_state.action_function is not None:
            next_state.action_function()
        self.set_state(next_state.current_state)
