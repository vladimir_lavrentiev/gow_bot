# -*- coding: utf-8 -*-
import sys
import time
import os, sys

from actions import action_battle
from application import App
from checks import *
from fsm import MainFsm
from loaders import LocationLoader, HeroesLoader
from tasks import Task
from transitions import *

BASE_FOLDER = 'Castles'
HEROES_FOLDER = 'Heroes'




mon = None


# TODO можно вынести в отдельный модуль, и, используя интроспекцию обернуть это всё в dict и по id получать имя
ON_MAP = 0
IN_CASTLE = 1
ADVENTURES_SELECTED = 11
# ADVENTURE_V_BOY               = 111
CHALLENGES_SELECTED = 12
# CHALLENGE_CONCRETE_SELECTED   = 121
EXPEDITION_SELECTED = 13
PREPARE_BATTLE_SELECTED = 2  # k_boyu
BATTLE = 3


class Bot:
    def __init__(self, location, task, location_images, heroes_images, initial_state=ON_MAP):
        self.task = task
        self.fsm = MainFsm()
        self.fill_fsm()
        self.fsm.set_state(initial_state)
        self.location = location
        self.location_images = location_images
        self.heroes_images = heroes_images

    def loop(self):
        while True:
            self.fsm.next()
            time.sleep(3)

    def fill_fsm(self):
        fsm = self.fsm
        task = self.task
        fsm.add_state(ON_MAP, IN_CASTLE, select_castle, check_in_castle)
        state = None
        if task == Task.ADVENTURES:
            state, select_action, verify = ADVENTURES_SELECTED, select_adventures, check_adv_selected
        elif task == Task.CHALLENGE:
            state, select_action, verify = CHALLENGES_SELECTED, select_challenges, check_challenge_selected
        elif task == Task.EXPEDITION:
            state, select_action, verify = EXPEDITION_SELECTED, select_expedition, check_expedition_selected
        else:
            print('Unknown action: "{}"!'.format(task))
            sys.exit()
        fsm.add_state(IN_CASTLE, state, select_action, verify)
        # for state in ADVENTURES_SELECTED, CHALLENGES_SELECTED, EXPEDITION_SELECTED:
        fsm.add_state(state, PREPARE_BATTLE_SELECTED, select_go_battle, check_prepare_battle)
        fsm.add_state(PREPARE_BATTLE_SELECTED, BATTLE, select_go_battle2,
                      check_battle_selected, None, action_battle)
        fsm.add_state(BATTLE, ON_MAP, None, None, None, None)  # TODO: fix it


def main(location, task):
    print("Execute in location '{}' with task '{}'".format(location, task))
    location_images = LocationLoader(location)
    heroes_images = HeroesLoader(HEROES_FOLDER)
    bot = Bot(location, task, location_images, heroes_images)
    bot.loop()


if __name__ == '__main__':
    main('FirePick', Task.CHALLENGE)
