# -*- coding: utf-8 -*-

from pywinauto import mouse
import time

from constants import *


def click(coords):
    print("click mouse ", coords)
    global rect
    middle_point = (int((coords[1][0]+coords[0][0])/2), int((coords[1][1]+coords[0][1])/2))
    left_shift = rect.left+ALL_WIN_SHIFT
    top_shift = rect.top+TOP_WIN_SHIFT
    x = middle_point[0]+left_shift
    y = middle_point[1]+top_shift
    mouse.click(coords=(x, y))


def swap_gems(rect, coord_from, coord_to):
    print("swap gems")
    left_shift = rect.left+BATTLE_AREA_LEFT+ALL_WIN_SHIFT
    top_shift = rect.top+BATTLE_AREA_TOP+TOP_WIN_SHIFT
    coords_from = left_shift+coord_from[1]*SIZE+HALF_SIZE, top_shift+coord_from[0]*SIZE+HALF_SIZE
    coords_to = left_shift+coord_to[1]*SIZE+HALF_SIZE, top_shift+coord_to[0]*SIZE+HALF_SIZE
    # print(coords_from, coords_to)
    mouse.press(coords=(coords_from))
    time.sleep(1)
    mouse.move(coords=(coords_to))
    time.sleep(1)
    mouse.release(coords=(coords_to))

