# -*- coding: utf-8 -*-

from enum import Enum


class Task(Enum):
    ADVENTURES = 1
    CHALLENGE = 2
    EXPEDITION = 3
