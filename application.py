# -*- coding: utf-8 -*-
import cv2
import mss
import mss.tools
import numpy as np
from pywinauto.application import Application

from constants import *


class App:
    def __init__(self, title="GemsofWar"):
        app = Application(backend="uia").connect(title=title)
        self.top_win = app.top_window()
        self.top_win.set_focus()
        self.rect = self.top_win.rectangle()

    def get_all_context(self, gray=False):
        self.top_win.set_focus()
        rect = self.top_win.rectangle()
        with mss.mss() as sct:
            # Part of the screen to capture
            roi = (rect.left + ALL_WIN_SHIFT + 2, rect.top + TOP_WIN_SHIFT, rect.right - ALL_WIN_SHIFT,
                   rect.bottom - ALL_WIN_SHIFT)
            # Get raw pixels from the screen, save it to a Numpy array
            img = np.array(sct.grab(roi))
            if gray:
                img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        return img

    def old(self):
        prev_frame_pix = np.zeros(shape)
        cycle_count = 0
        with mss.mss() as sct:
            # Part of the screen to capture
            while "Screen capturing":
                cycle_count += 1
                last_time = time.time()
                rect = top_win.rectangle()
                mon = (rect.left + ALL_WIN_SHIFT + 2, rect.top + TOP_WIN_SHIFT, rect.right - ALL_WIN_SHIFT,
                       rect.bottom - ALL_WIN_SHIFT)
                # Get raw pixels from the screen, save it to a Numpy array
                img = np.array(sct.grab(mon))
                img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                if cycle_count % 3 == 0:
                    btn = detect_any_button(img_gray, templates)
                    if btn is not None:
                        click(btn)
                        time.sleep(5)
                        continue
                # height,width = img_gray.shape
                # print(height, width)
                battle(img_gray)
                time_delta = time.time() - last_time
                # print("real fps: {}".format(1 / time_delta))
                if time_delta < 1 / TARGET_FPS:
                    time.sleep(1 / TARGET_FPS - time_delta)


app = App()
