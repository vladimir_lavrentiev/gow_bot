# -*- coding: utf-8 -*-
import cv2
import numpy as np

from constants import SIZE


def solve(list_of_coordinates):
    a = np.asarray(list_of_coordinates)
    row_coords = a[:, 0]
    # print("row_coords", row_coords)
    col_coords = a[:, 1]
    # print("col_coords", col_coords)
    decision = list_of_coordinates._solve(row_coords, col_coords)
    if decision is not None:
        return decision
    print("ROTATE_BOARD")
    decision = list_of_coordinates._solve(col_coords, row_coords)
    if decision is None:
        print("DECISION NOT FOUND!")
        return None, None
    old, new = decision
    return ((old[1], old[0]), (new[1], new[0]))


def _solve(row_coords, col_coords):
    lst = list(zip(row_coords, col_coords))
    # print("initial list", lst)
    rows_uniq, rows_count = np.unique(row_coords, return_counts=True)
    # print("rows_uniq", rows_uniq)
    # print("rows_count", rows_count)
    cols_uniq, cols_count = np.unique(col_coords, return_counts=True)
    # print("cols_uniq", cols_uniq)
    # print("cols_count", cols_count)
    rows_distrib = dict(zip(rows_uniq, rows_count))
    # print("rows_distrib", rows_distrib)
    cols_distrib = dict(zip(cols_uniq, cols_count))
    # print("cols_distrib", cols_distrib)
    rows_cnt = rows_uniq.size
    # print("rows_cnt", rows_cnt)
    cols_cnt = cols_uniq.size
    # print("cols_cnt", cols_cnt)
    row_min = np.amin(rows_uniq)
    row_max = np.amax(rows_uniq)
    col_min = np.amin(cols_uniq)
    col_max = np.amax(cols_uniq)
    # print("row_min", row_min, "row_max", row_max)
    # print("col_min", col_min, "col_max", col_max)
    main_row = rows_uniq[np.argmax(rows_count)]
    main_col = cols_uniq[np.argmax(cols_count)]
    # print("main row ", main_row)
    # print("main col ", main_col)
    if rows_cnt == 1:
        print("one line detected")
        for x in range(col_min, col_max + 1):
            if x not in cols_uniq:
                break
        if (x - col_min == 1):
            return ((row_min, col_min), (row_min, x))
        elif col_max - x == 1:
            return ((row_min, x), (row_min, col_max))
        else:
            return None
    elif rows_cnt == 2 and cols_cnt > 2:
        print("G-style detected")
        second_row = rows_uniq[np.argmin(rows_count)]
        # print("main row ", main_row)
        # print("second row ", second_row)
        tuple_with_second_row = [x for x in lst if x[0] == second_row]
        # print("tuple", tuple_with_second_row)
        col = tuple_with_second_row[0][1]
        old = (second_row, col)
        new = (main_row, col)
        # print("old", old)
        # print("new", new)
        return (old, new)
    elif rows_cnt == 3 and cols_cnt == 4:
        print("===Figure 3x4 detected===")
        col_from = col_min if (main_row, col_min + 1) not in lst else col_max
        # print("col_from", col_from)
        old = (main_row, col_from)
        new = (main_row, main_col)
        # print("old", old)
        # print("new", new)
        return (old, new)
    elif rows_cnt == 4 and cols_cnt == 4:
        print("===Figure 4x4 detected===")
        col_from = col_min if (main_row, col_min + 1) not in lst else col_max
        # print("col_from", col_from)
        old = (main_row, col_from)
        new = (main_row, main_col)
        return (old, new)
    return None


def calc_pix(im, cnt):
    shape = (cnt, cnt)
    e = np.zeros(shape)
    for i in range(cnt):
        # print ("\n")
        for j in range(cnt):
            part = im[i * SIZE + 1:(i + 1) * SIZE - 1, j * SIZE + 1:(j + 1) * SIZE - 1]
            e[i, j] = cv2.countNonZero(part)
            # print (e[i,j])
    print(e)
    return e


def detect_big_objects(prev_frame_pix, current_frame_pix, atol=0, rtol=0.1):
    diff = np.isclose(prev_frame_pix, current_frame_pix, atol=atol, rtol=rtol)
    return np.where(diff == False)
