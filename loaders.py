# -*- coding: utf-8 -*-
import glob
import os

import cv2


class Loader:

    @staticmethod
    def load_images(base_folder):
        images = {}
        fnames = glob.glob(base_folder + '/*.png')
        # print(fnames)
        for fname in fnames:
            img = cv2.imread(fname, 0)
            images[fname] = img
            print(fname)
        return images


class LocationLoader:
    def __init__(self, location_folder):
        # input_dir = os.path.join(location_folder)
        self.templates = Loader.load_images(location_folder)


class HeroesLoader:
    def __init__(self, heroes_folder):
        # input_dir = os.path.join(location_folder)
        self.templates = Loader.load_images(heroes_folder)
