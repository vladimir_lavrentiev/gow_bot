# -*- coding: utf-8 -*-
import inspect
import time

from brains import calc_pix, detect_big_objects, solve
from constants import *
from utils import swap_gems
from application import app

def battle():
    global prev_frame_pix
    img_gray = app.get_all_context(gray=True)
    balls_area = img_gray[BATTLE_AREA_TOP:BATTLE_AREA_TOP + BATTLE_AREA_SIZE,
                 BATTLE_AREA_LEFT:BATTLE_AREA_LEFT + BATTLE_AREA_SIZE]
    ret, bw_img = cv2.threshold(balls_area, 125, 255, cv2.THRESH_BINARY)
    # cv2.imshow('thresh',balls_area)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    current_frame_pix = calc_pix(bw_img, BALLS_CNT)
    if (prev_frame_pix == np.zeros(shape)).all():
        print("======")
        prev_frame_pix = current_frame_pix
    else:
        big_x, big_y = detect_big_objects(prev_frame_pix, current_frame_pix, atol=25, rtol=0.08)
        list_of_coordinates = list(zip(big_x, big_y))
        if len(list_of_coordinates) == 0:
            return
        print(list_of_coordinates)
        if len(list_of_coordinates) >= 3:
            _from, _to = solve(list_of_coordinates)
            if _from is None:
                prev_frame_pix = np.zeros(shape)
                return
            if len(list_of_coordinates) > 3:
                swap_gems(rect, _from, _to)
                time.sleep(5)
                prev_frame_pix = np.zeros(shape)
            else:
                try_to_use_skills()
                swap_gems(rect, _from, _to)
                time.sleep(5)
                prev_frame_pix = np.zeros(shape)
        else:
            print("Error detecting of balls")

def action_battle():
    print(inspect.stack()[0][3])



def try_to_use_skills():
    global mon
    thresh = 0.01
    print("try to use skills")
    templ = [template for template_name, template in templates.items() if "apply" in template_name][0]
    w, h = templ.shape[::-1]
    method = cv2.TM_CCORR_NORMED
    for i in range(4):
        coords = (110, 200+160*i)
        mouse.click(coords=coords)
        img = np.array(sct.grab(mon))
        img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        res = cv2.matchTemplate(img_gray,templ,method)
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
        print("apply=", min_val, max_val)
        if abs(1-max_val) < thresh:
            if method in [cv2.TM_SQDIFF, cv2.TM_SQDIFF_NORMED]:
                top_left = min_loc
            else:
                top_left = max_loc
            bottom_right = (top_left[0] + w, top_left[1] + h)
            # print("button detected - ", template_name)
            # print("top left", top_left)
            coords = top_left[0]+10, top_left[1]+10
            print("img top left", img[coords])
            # if ()
            click((top_left, bottom_right))
        time.sleep(3)
        mouse.click(coords=coords)