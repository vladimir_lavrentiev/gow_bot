# -*- coding: utf-8 -*-
import inspect


def check_on_map():
    print(inspect.stack()[0][3])


def check_in_castle():
    print(inspect.stack()[0][3])
    return True


def check_adv_selected():
    print(inspect.stack()[0][3])
    return True


def check_challenge_selected():
    print(inspect.stack()[0][3])
    return True


def check_expedition_selected():
    print(inspect.stack()[0][3])
    return True


def check_prepare_battle():
    print(inspect.stack()[0][3])
    return True


def check_battle_selected():
    print(inspect.stack()[0][3])
    return True


def detect_any_button(img, templates):
    print("detect buttons")
    thresh = 0.005
    for template_name, template in templates.items():
        # res = cv2.matchTemplate(img,template,cv2.TM_CCOEFF_NORMED)
        # methods = ['cv2.TM_CCOEFF', 'cv2.TM_CCOEFF_NORMED', 'cv2.TM_CCORR', 'cv2.TM_CCORR_NORMED', 'cv2.TM_SQDIFF', 'cv2.TM_SQDIFF_NORMED']
        methods = [cv2.TM_CCORR_NORMED, ]
        for method in methods:
            w, h = template.shape[::-1]
            # Apply template Matching
            res = cv2.matchTemplate(img, template, method)
            min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
            print(template_name, min_val, max_val)
            if abs(1 - max_val) < thresh:
                # If the method is TM_SQDIFF or TM_SQDIFF_NORMED, take minimum
                if method in [cv2.TM_SQDIFF, cv2.TM_SQDIFF_NORMED]:
                    top_left = min_loc
                else:
                    top_left = max_loc
                bottom_right = (top_left[0] + w, top_left[1] + h)
                # print("button detected - ", template_name)
                return ((top_left, bottom_right))
            # cv2.rectangle(img,top_left, bottom_right, 255, 2)

            # plt.subplot(121),plt.imshow(res,cmap = 'gray') #, figsize=(12,12)
            # plt.title('Matching Result'), plt.xticks([]), plt.yticks([])
            # plt.subplot(122),plt.imshow(img,cmap = 'gray')
            # plt.title('Detected Point'), plt.xticks([]), plt.yticks([])
            # plt.suptitle('aaa')

            # plt.show()
    return None